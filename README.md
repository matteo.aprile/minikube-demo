# kubernetes-demo

## Data di Minikube:

per runnarlo:
```
$ minikube start
😄  minikube v1.30.1 on Darwin 12.5 (arm64)
✨  Using the docker driver based on existing profile
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔄  Restarting existing docker container for "minikube" ...
🐳  Preparing Kubernetes v1.26.3 on Docker 23.0.2 ...
🔗  Configuring bridge CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default

$ minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

per vedere i vari dati:
```
$ kubectl get pod
No resources found in default namespace.

$ kubectl apply -f mongo-config.yaml 
configmap/mongo-config created

$ kubectl apply -f mongo-secret.yaml 
secret/mongo-secret created

$ kubectl apply -f mongo.yaml 
deployment.apps/mongo-deployment created
service/mongo-service created

$ kubectl apply -f webapp.yaml 
deployment.apps/webapp-deployment created
service/webapp-service created

$ kubectl get all
NAME                                    READY   STATUS              RESTARTS   AGE
pod/mongo-deployment-855b879886-6j8xl   0/1     ContainerCreating   0          67s
pod/webapp-deployment-9fccd564d-bs6vf   0/1     ContainerCreating   0          53s

NAME                     TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP          64m
service/mongo-service    ClusterIP   10.107.66.104   <none>        27017/TCP        67s
service/webapp-service   NodePort    10.100.97.122   <none>        3000:30100/TCP   53s

NAME                                READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/mongo-deployment    0/1     1            0           67s
deployment.apps/webapp-deployment   0/1     1            0           53s

NAME                                          DESIRED   CURRENT   READY   AGE
replicaset.apps/mongo-deployment-855b879886   1         1         0       67s
replicaset.apps/webapp-deployment-9fccd564d   1         1         0       53s

$ kubectl get configmap
NAME               DATA   AGE
kube-root-ca.crt   1      64m
mongo-config       1      2m7s

$ kubectl get secret
NAME           TYPE     DATA   AGE
mongo-secret   Opaque   2      2m4s

$ kubectl describe service webapp-service
Name:                     webapp-service
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=webapp
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.100.97.122
IPs:                      10.100.97.122
Port:                     <unset>  3000/TCP
TargetPort:               3000/TCP
NodePort:                 <unset>  30100/TCP
Endpoints:                <none>
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

$ kubectl describe pod mongo-deployment-855b879886-6j8xl
Name:             mongo-deployment-855b879886-6j8xl
Namespace:        default
Priority:         0
Service Account:  default
Node:             minikube/192.168.49.2
Start Time:       Sun, 21 May 2023 20:11:55 +0200
Labels:           app=mongo
                  pod-template-hash=855b879886
Annotations:      <none>
Status:           Pending
IP:               
IPs:              <none>
Controlled By:    ReplicaSet/mongo-deployment-855b879886
Containers:
  mongodb:
    Container ID:   
    Image:          mongo:5.0
    Image ID:       
    Port:           27017/TCP
    Host Port:      0/TCP
    State:          Waiting
      Reason:       ContainerCreating
    Ready:          False
    Restart Count:  0
    Environment:
      MONGO_INITDB_ROOT_USERNAME:  <set to the key 'mongo-user' in secret 'mongo-secret'>      Optional: false
      MONGO_INITDB_ROOT_PASSWORD:  <set to the key 'mongo-password' in secret 'mongo-secret'>  Optional: false
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-4n6jc (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  kube-api-access-4n6jc:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  3m42s  default-scheduler  Successfully assigned default/mongo-deployment-855b879886-6j8xl to minikube
  Normal  Pulling    3m42s  kubelet            Pulling image "mongo:5.0"

$ kubectl get pod
NAME                                READY   STATUS              RESTARTS   AGE
mongo-deployment-855b879886-6j8xl   0/1     ContainerCreating   0          2m3s
webapp-deployment-9fccd564d-bs6vf   0/1     ContainerCreating   0          109s

$ kubectl logs webapp-deployment-9fccd564d-bs6vf
app listening on port 3000!
```

per effettuare l'accesso via browser:
```
$ kubectl get node -o wide
NAME       STATUS   ROLES           AGE   VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
minikube   Ready    control-plane   70m   v1.26.3   192.168.49.2   <none>        Ubuntu 20.04.5 LTS   5.15.49-linuxkit   docker://23.0.2
```

url: `http://192.168.49.2:30100`

## Argo CD

### installazione
installazione Argo CD nel cluster
```
$ kubectl create namespace argocd
namespace/argocd created

$ kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

per vedere tutti i pods creati per ArgoCD
```
$ kubectl get pod -n argocd
NAME                                               READY   STATUS    RESTARTS   AGE
argocd-application-controller-0                    1/1     Running   0          2m28s
argocd-applicationset-controller-86fc5c85-vm9dz    1/1     Running   0          2m28s
argocd-dex-server-7f4689df5-qvs8v                  1/1     Running   0          2m28s
argocd-notifications-controller-59f78959c8-lxb8r   1/1     Running   0          2m28s
argocd-redis-74cb89f466-qhslf                      1/1     Running   0          2m28s
argocd-repo-server-6578ccfc67-sjbc6                1/1     Running   0          2m28s
argocd-server-854c79df45-6f5tf                     1/1     Running   0          2m28s

$ kubectl get svc -n argocd
NAME                                    TYPE      CLUSTER-IP     EXTERNAL-IP PORT(S)                    AGE
argocd-applicationset-controller        ClusterIP 10.111.167.207 <none>      7000/TCP,8080/TCP          4m7s
argocd-dex-server                       ClusterIP 10.105.37.139  <none>      5556/TCP,5557/TCP,5558/TCP 4m7s
argocd-metrics                          ClusterIP 10.99.128.255  <none>      8082/TCP                   4m7s
argocd-notifications-controller-metrics ClusterIP 10.109.217.98  <none>      9001/TCP                   4m7s
argocd-redis                            ClusterIP 10.104.175.179 <none>      6379/TCP                   4m7s
argocd-repo-server                      ClusterIP 10.96.38.212   <none>      8081/TCP,8084/TCP          4m7s
argocd-server                           ClusterIP 10.109.35.15   <none>      80/TCP,443/TCP             4m7s
argocd-server-metrics                   ClusterIP 10.102.102.167 <none>      8083/TCP                   4m7s
```

per inoltrare la richiesta del server a localhost:
```
kubectl port-forward -n argocd svc/argocd-server 8080:443
```

con credenziali:

- username: admin
- password autogenerata:
```
$ kubectl get secret argocd-initial-admin-secret -n argocd -o yaml
```

### config
apply ArgoCD application.yaml:
```
$ kubectl apply -f application.yaml
application.argoproj.io/minikube-demo-argo-app created
```